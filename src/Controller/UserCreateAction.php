<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\User\UserFactory;
use App\Component\User\UserManager;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class UserCreateAction extends AbstractController
{
    public function __construct(
        private readonly UserFactory $userFactory,
        private readonly UserManager $userManager,
        private readonly MailerInterface $mailer
    ){
    }

    public function __invoke(User $data): User
    {
        $user = $this->userFactory->create($data->getEmail(), $data->getPassword(), $data->getAge());
        $this->userManager->save($user, true);

        $email = (new Email())
            ->from('eagledev_tank@mail.ru')
            ->to($user->getEmail())
            ->subject('Bu tizimga kirish xati')
            ->html('Siz tizimdan royxatdan otdingiz!!! <br /><strong>http://localhost:8080/</strong>');

        $this->mailer->send($email);

        return $user;
    }
}
